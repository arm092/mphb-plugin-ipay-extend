<?php

namespace MPHB\Payments\Gateways\Arca;

use MPHB\Payments\Gateways\AbstractNotificationListener;
use MPHB\PostTypes\PaymentCPT\Statuses as PaymentStatuses;

class ArcaListener extends AbstractNotificationListener
{
    protected $username = '';
    protected $password = '';
    protected $redirect = '';

    /**
     * Payment instance form validate method
     *
     * @var \MPHB\Entities\Payment|null
     */
    protected $payment = null;

    public function __construct($atts)
    {
        $this->username = $atts['username'];
        $this->password = $atts['password'];

        parent::__construct($atts);
    }

    protected function initUrlIdentificationValue()
    {
        return 'arca';
    }

    protected function parseInput()
    {
        return empty($_GET) ? [] : wp_unslash($_GET);
    }

    protected function validate($payload)
    {
        $this->payment = MPHB()->getPaymentRepository()->findById((int) $payload['payment_id']);
        $isNice = (bool) $this->payment && in_array($this->payment->getStatus(), [
                PaymentStatuses::STATUS_ON_HOLD,
                PaymentStatuses::STATUS_PENDING,
            ], true);

        if ($isNice) {
            if (empty($this->payment->getTransactionId()) && !empty($payload['orderId'])) {
                $this->payment->setTransactionId($payload['orderId']);
            } elseif ($this->payment->getTransactionId()=='') {
                $isNice = false;
                $this->paymentOnHold(__('There is no Transaction ID for payment','motopress-hotel-booking' ));
            }
        }

        return $isNice;
    }

    /**
     * @return \MPHB\Entities\Payment|null
     */
    protected function retrievePayment()
    {
        if ($this->payment) {
            return $this->payment;
        }

        return MPHB()->getPaymentRepository()->findOne(['payment_key' => $this->input['payment_key']]);
    }

    protected function process()
    {
        $this->paymentOnHold('');
        $response = MPHB()->gatewayManager()->getGateway($this->gatewayId)->paymentStatus($this->payment);

        if ((isset($response['errorMessage']) && $response['errorMessage']!=='Success') || !isset($response['orderStatus'])) {
            $this->paymentFailed(__($response['errorMessage'], 'motopress-hotel-booking'));
            wp_redirect(esc_url_raw(MPHB()->settings()->pages()->getPaymentFailedPageUrl($this->payment)));
            exit();
        }

        switch ($response['orderStatus']) {
            case '0':
                // Заказ зарегистрирован, но не оплачен
                $this->paymentOnHold(__('Заказ зарегистрирован, но не оплачен','motopress-hotel-booking'));
                $this->redirect = MPHB()->settings()->pages()->getBookingConfirmedPageUrl();
                break;
            case '1':
                // Деньги были захолдированы на карте (для двухстадийных платежей)
                $this->paymentOnHold(__('Деньги были захолдированы на карте','motopress-hotel-booking'));
                $this->redirect = MPHB()->settings()->pages()->getBookingConfirmedPageUrl();
                break;
            case '2':
                // Проведена полная авторизация суммы заказа
                $this->paymentCompleted(__('Проведена полная авторизация суммы заказа','motopress-hotel-booking'));
                $this->redirect = MPHB()->settings()->pages()->getReservationReceivedPageUrl();
                break;
            case '3':
                // Авторизация отменена
                $this->paymentFailed(__('Авторизация отменена','motopress-hotel-booking'));
                $this->redirect = MPHB()->settings()->pages()->getPaymentFailedPageUrl();
                break;
            case '4':
                // По транзакции была проведена операция возврата
                $this->paymentRefunded(__('По транзакции была проведена операция возврата','motopress-hotel-booking'));
                $this->redirect = MPHB()->settings()->pages()->getUserCancelRedirectPageUrl();
                break;
            case '5':
                // Инициирована авторизация через ACS банка-эмитента
                $this->paymentOnHold(__('Инициирована авторизация через ACS банка-эмитента','motopress-hotel-booking'));
                $this->redirect = MPHB()->settings()->pages()->getBookingConfirmedPageUrl();
                break;
            case '6':
                // Авторизация отклонена
                $this->paymentFailed(__('Авторизация отклонена','motopress-hotel-booking'));
                $this->redirect = MPHB()->settings()->pages()->getPaymentFailedPageUrl();
                break;
        }
    }

    public function fireExit($succeed)
    {
        if ($succeed && $this->redirect) {
            http_response_code(200);
            wp_redirect(esc_url_raw($this->redirect));
        } else {
            http_response_code(400);
            wp_redirect('/');
            parent::fireExit($succeed);
        }

    }

    protected function paymentCompleted($log)
    {

        return MPHB()->paymentManager()->completePayment($this->payment, $log);
    }

    protected function paymentRefunded($log)
    {
        return MPHB()->paymentManager()->refundPayment($this->payment, $log);
    }

    protected function paymentFailed($log)
    {
        return MPHB()->paymentManager()->failPayment($this->payment, $log);
    }

    protected function paymentOnHold($log)
    {
        return MPHB()->paymentManager()->holdPayment($this->payment, $log);
    }
}
