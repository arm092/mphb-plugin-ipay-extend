<?php

namespace MPHB\Payments\Gateways;

use \MPHB\Admin\Groups;
use \MPHB\Admin\Fields;
use MPHB\Entities\Payment;

class ArcaGateway extends Gateway
{
    /**
     *
     * @var Arca\ArcaListener
     */
    protected $listener;

    /**  @var string */
    protected $username;

    /** @var string */
    protected $password;

    /** @var string */
    protected $username_sandbox;

    /**  @var string */
    protected $password_sandbox;

    /** @var array */
    protected $supportedCurrencies = [
        'RUB' => '643',
        'USD' => '840',
        'AMD' => '051',
    ];


    public function __construct()
    {
        add_filter('mphb_gateway_has_instructions', [$this, 'hideInstructions'], 10, 2);

        parent::__construct();

        $this->setupNotificationListener();
    }

    protected function setupNotificationListener()
    {
        $args = [
            'gatewayId' => $this->getId(),
            'sandbox'   => $this->isSandbox,
            'username'  => $this->getUsername(),
            'password'  => $this->getPassword(),
        ];

        $this->listener = new Arca\ArcaListener($args);
    }

    /**
     * @param  bool    $show
     * @param  string  $gatewayId
     *
     * @return bool
     */
    public function hideInstructions($show, $gatewayId)
    {
        if ($gatewayId == $this->id) {
            $show = false;
        }

        return $show;
    }

    protected function setupProperties()
    {
        parent::setupProperties();
        $this->adminTitle       = __('Arca (iPay)', 'motopress-hotel-booking');
        $this->username         = urlencode($this->getOption('username'));
        $this->password         = urlencode($this->getOption('password'));
        $this->username_sandbox = urlencode($this->getOption('username_sandbox'));
        $this->password_sandbox = urlencode($this->getOption('password_sandbox'));

        if ($this->isSandbox) {
            $this->description .= ' ' . sprintf(__('Use the card number %1$s with CVC %2$s and a %3$s expiration date to test a payment.', 'motopress-hotel-booking'), '5406610008745130', '651', '12/20');
            $this->description = trim($this->description);
        }
    }

    protected function initDefaultOptions()
    {
        $defaults = [
            'title'            => __('Pay by Card', 'motopress-hotel-booking'),
            'description'      => __('Make your payment online via your credit or debit card (Arca/Visa/Master).', 'motopress-hotel-booking'),
            'enabled'          => false,
            'is_sandbox'       => false,
            'username'         => '',
            'password'         => '',
            'username_sandbox' => '',
            'password_sandbox' => '',
        ];

        return array_merge(parent::initDefaultOptions(), $defaults);
    }


    protected function initId()
    {
        return 'arca';
    }

    /**
     * Get the status of payment from processing
     *
     * @param  Payment  $payment
     *
     * @return array|null
     */
    public function paymentStatus(\MPHB\Entities\Payment $payment)
    {
        $args = [
            'userName' => $this->getUsername(),
            'password' => $this->getPassword(),
            'orderId'  => $payment->getTransactionId(),
            'language' => wpm_get_language(),
        ];

        return $response = $this->sendRequest('/payment/rest/getOrderStatusExtended.do', 'POST', $args);
    }

    /**
     *
     * @param  \MPHB\Entities\Booking  $booking
     * @param  \MPHB\Entities\Payment  $payment
     */
    public function processPayment(\MPHB\Entities\Booking $booking, \MPHB\Entities\Payment $payment)
    {
        $url = $this->getPaymentUrl($booking, $payment);

        // Redirect to arca checkout
        wp_redirect($url);
        exit;
    }

    /**
     * @param  Payment  $payment
     *
     * @return bool
     */
    public function makeRefund($payment, $amount)
    {
        $partial = $amount != $payment->getAmount();

        $args = [
            'userName' => $this->getUsername(),
            'password' => $this->getPassword(),
            'orderId'  => $payment->getTransactionId(),
            'amount'   => $amount * 100,
        ];

        $response = $this->sendRequest('/payment/rest/refund.do', 'POST', $args);
        if (isset($response['errorCode']) && $response['errorCode'] == 0) {
            $total = $payment->getAmount();
            $left  = $total - $amount;
            if ($partial) {
                $response = $this->paymentStatus($payment);
                if (isset($response['errorCode']) && $response['errorCode'] == 0 && isset($response['paymentAmountInfo']['depositedAmount'])) {
                    $left = ((float) $response['paymentAmountInfo']['depositedAmount']) / 100;
                }
                $payment->setAmount($left);
                MPHB()->getPaymentRepository()->save($payment);
            }
            $log = sprintf(__('Refunds in the amount of %1$s drams were made from %2$s drams, the rest of the total amount: %3$s drams', 'motopress-hotel-booking'), $amount, $total, $left);

            return $this->paymentRefunded($payment, $log);
        }
        $payment->addLog(isset($response['errorMessage']) ? $response['errorMessage'] : 'Something went wrong during refund operation');

        return MPHB()->getPaymentRepository()->save($payment);

    }

    /**
     *
     * @param  \MPHB\Entities\Payment  $payment
     *
     * @return boolean
     */
    protected function paymentRefunded($payment, $log = '')
    {
        return MPHB()->paymentManager()->refundPayment($payment, $log, true);
    }

    /**
     * Get the PayPal request URL for an booking.
     *
     * @param  \MPHB\Entities\Booking  $booking
     * @param  \MPHB\Entities\Payment  $payment
     *
     * @return string
     */
    public function getPaymentUrl($booking, $payment)
    {
        $args     = $this->getRequestArgs($booking, $payment);
        $response = $this->sendRequest('/payment/rest/register.do', 'POST', $args);

        if (isset($response['errorMessage']) || empty($response['formUrl'])) {
            $payment->addLog(__($response['errorMessage'], 'motopress-hotel-booking'));
            $this->paymentFailed($payment);

            return esc_url_raw(MPHB()->settings()->pages()->getPaymentFailedPageUrl($payment));
        }
        //Not set sometime
        $payment->setTransactionId($response['orderId']);
        $payment->setEmail($booking->getCustomer()->getEmail());
        MPHB()->getPaymentRepository()->save($payment);

        return $response['formUrl'];
    }

    /**
     *
     * @param  \MPHB\Entities\Booking  $booking
     * @param  \MPHB\Entities\Payment  $payment
     *
     * @return array
     */
    public function getRequestArgs($booking, $payment)
    {
        $description = get_bloginfo('name') . " Booking\n";
        foreach ($booking->getReservedRooms() as $room) {
            $title       = MPHB()->getRoomTypeRepository()->findById($room->getRoomTypeId())->getTitle();
            $description .= "Room Type: " . $title . "\n";
        }

        return [
            'userName'    => $this->getUsername(),
            'password'    => $this->getPassword(),
            'orderNumber' => ($this->isSandbox ? 'alaska_r_' : '') . $booking->getId(),
            'amount'      => $payment->getAmount() * 100,
            'currency'    => $this->supportedCurrencies[$payment->getCurrency()],
            'returnUrl'   => esc_url_raw(MPHB()->settings()->pages()
                                               ->getReservationReceivedPageUrl($payment, [
                                                   'mphb_payment_status' => 'auto',
                                                   'mphb-listener'       => 'arca',
                                               ])),
            'description' => $description,
            'language'    => wpm_get_language(),
            'clientId'    => $booking->getCustomer()->getEmail(),
            'pageView'    => $this->isMobile() ? 'MOBILE' : 'DESKTOP',

        ];
    }

    /**
     * Send CURL request to processing
     *
     * @param  string  $uri
     * @param  string  $method
     * @param  array   $args
     *
     * @return array|null
     */
    public function sendRequest($uri, $method = 'GET', $args = [])
    {
        if ($this->isSandbox) {
            $host = 'https://ipaytest.arca.am:8445';
        } else {
            $host = 'https://ipay.arca.am';
        }
        $url = $host . $uri;
        $ch  = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        return json_decode($response, true);
    }

    /**
     *
     * @param  \MPHB\Admin\Tabs\SettingsSubTab  $subTab
     */
    public function registerOptionsFields(&$subTab)
    {
        parent::registerOptionsFields($subTab);
        $group = new Groups\SettingsGroup("mphb_payments_{$this->id}_group2", '', $subTab->getOptionGroupName());

        $groupFields = [
            Fields\FieldFactory::create("mphb_payment_gateway_{$this->id}_username", [
                'type'    => 'text',
                'label'   => __('Arca API username', 'motopress-hotel-booking'),
                'default' => $this->getDefaultOption('username'),
            ]),
            Fields\FieldFactory::create("mphb_payment_gateway_{$this->id}_password", [
                'type'    => 'text',
                'label'   => __('Arca API password', 'motopress-hotel-booking'),
                'default' => $this->getDefaultOption('password'),
            ]),
            Fields\FieldFactory::create("mphb_payment_gateway_{$this->id}_username_sandbox", [
                'type'    => 'text',
                'label'   => __('Sandbox username', 'motopress-hotel-booking'),
                'default' => $this->getDefaultOption('username_sandbox'),
            ]),
            Fields\FieldFactory::create("mphb_payment_gateway_{$this->id}_password_sandbox", [
                'type'    => 'text',
                'label'   => __('Sandbox password', 'motopress-hotel-booking'),
                'default' => $this->getDefaultOption('password_sandbox'),
            ]),
        ];

        $group->addFields($groupFields);

        $subTab->addGroup($group);
    }

    /**
     * Username getter
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->isSandbox ? $this->username_sandbox : $this->username;
    }

    /**
     * Password getter
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->isSandbox ? $this->password_sandbox : $this->password;
    }

    /**
     * Generate supported currencies array
     *
     * @return void
     */
    private function setupSupportedCurrencies()
    {
        $supportedCurrencies       = apply_filters('mphb_arca_supported_currencies', $this->supportedCurrencies);
        $this->supportedCurrencies = $supportedCurrencies;
    }

    /**
     * Determine if the currency is supported
     *
     * @return bool
     */
    public function isSupportCurrency()
    {
        return in_array(MPHB()->settings()->currency()->getCurrencyCode(), $this->supportedCurrencies);
    }

    /**
     * Determine if the client browser is mobile
     *
     * @return bool
     */
    public function isMobile()
    {
        $useragent = $_SERVER['HTTP_USER_AGENT'];

        return preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4));
    }
}
